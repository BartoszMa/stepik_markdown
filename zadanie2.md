| kolumna1    | kolumna 2   |    kolumna3   |
|    :----:   |    :----:   |    :----:     |
| podpunkt1.1 | podpunkt2.1 | podpunkt3.1   |
| podpunkt1.2 | podpunkt2.2 | podpunkt3.2   |


# python

```python
print("hello world")
print("hello world")

```

Here's a simple footnote,[^1]

[python](#python)

- [x] Kupić mleko
- [ ] Zjeść kolacje
- [ ] Wypić dwa ==litry wody== :smirk: :smirk:

==Pozdrawiam==
